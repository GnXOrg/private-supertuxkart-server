#!/usr/bin/python3

from shutil import which
from pathlib import Path
from colorama import init, deinit
from utils import *
import subprocess
import sys
import requests
import time

print("Cross-platform SuperTuxKart server launcher by GnXOrg (tretrauit/darkelaina).")

def main():
    no_chisel = False
    no_ngrok = False

    init() # initialize colorama
    cfg = config()
    # Check if configuration is empty
    if len(cfg.config) == 0:
        cfg.config = cfg.default()
        cfg.write()
        error("New configuration file created, please edit the file and relaunch.")
        sys.exit(-1)

    if which(cfg.config["stk"]["exec"]) or Path(cfg.config["chisel"]["exec"]).exists():
        info(f'Found STK executable: {cfg.config["stk"]["exec"]}')
    else:
        if not cfg.config["stk"]["auto_dl"]:
            error("""STK not found, please specify the executable path in config.json. If you want to download then change 'auto_dl' to true""")
            sys.exit(-2)
        else:
            warn("STK doens't exist, downloading...")
            dl_stk()

    if which(cfg.config["chisel"]["exec"]) or Path(cfg.config["chisel"]["exec"]).exists():
        info(f'Found chisel executable: {cfg.config["chisel"]["exec"]}')
    elif cfg.config["chisel"]["exec"] == "":
        warn("Chisel disabled, will not use Chisel to convert UDP traffic to HTTP.")
        no_chisel = True
    else:
        if not cfg.config["chisel"]["auto_dl"]:
            error("""Chisel not found, please specify the executable path in config.json. If you want to download then change 'auto_dl' to true""")
            sys.exit(-3)
        else:
            warn("Chisel doens't exist, downloading...")
            dl_chisel()

    if which(cfg.config["ngrok"]["exec"]) or Path(cfg.config["chisel"]["exec"]).exists():
        info(f'Found ngrok executable: {cfg.config["ngrok"]["exec"]}')
    elif cfg.config["ngrok"]["exec"] == "" or no_chisel:
        warn("ngrok disabled, will not use ngrok expose local server.")
        no_ngrok = True
    else:
        if not cfg.config["ngrok"]["auto_dl"]:
            error("""ngrok not found, please specify the executable path in config.json. If you want to download then change 'auto_dl' to true""")
            sys.exit(-4)
        else:
            warn("ngrok doens't exist, downloading...")
            raise NotImplementedError("Ngrok download is not implemented due to not having an API for that thing.")

    # Prepare logging
    info("Preparing logging for the server...")
    log_path = Path("./log/")
    log_path.mkdir(parents=True, exist_ok=True)
    stk_log = log_path.joinpath("stk.log").open("w+")
    chisel_log = log_path.joinpath("chisel.log").open("w+")
    ngrok_log = log_path.joinpath("ngrok.log").open("w+")

    # Start processes
    info("Starting STK server...")
    # Whats the point of this script if we disable STK server starting :p
    stk_proc = subprocess.Popen([cfg.config["stk"]["exec"], "--server-config=config.xml", 
    '--lan-server=Vietnamese-Internet', '--network-console'], stdout=stk_log, stderr=subprocess.STDOUT)
    chisel_proc = None
    if not no_chisel:
        info("Starting chisel tunnel...")
        chisel_proc = subprocess.Popen([cfg.config["chisel"]["exec"], "server", "--host", "0.0.0.0"], stdout=chisel_log, stderr=subprocess.STDOUT)
    ngrok_proc = None
    if not no_ngrok:
        info("Starting ngrok tunnel...")
        ngrok_proc = subprocess.Popen([cfg.config["ngrok"]["exec"], "http", "--region", "in", "8080"], stdout=ngrok_log, stderr=subprocess.STDOUT)
        time.sleep(2)
    info("SuperTuxKart server started sucessfully.")
    if not no_ngrok:
        ngrok_rsp = requests.get("http://localhost:4040/api/tunnels")
        if ngrok_rsp.status_code == 200:
            ngrok_json = ngrok_rsp.json()
            print(f'The *chisel* server address is: {ngrok_json["tunnels"][0]["public_url"]}')
            warn("!!!OTHERS NEED TO CONNECT TO THIS SERVER THROUGH CHISEL CLIENT OR IT WILL NOT WORK!!!")
            warn(f"Connect to the server by executing: chisel client {ngrok_json['tunnels'][0]['public_url']} localhost:2759/udp")
            warn("then after that access the actual STK server by address: localhost:2579")
            warn("NOTE: If you're in the local network then you can connect to STK server directly")
            warn("by address localhost:2759 without having to connect using chisel client.")
    else:
        if no_chisel:
            warn("You need to connect to the server using: localhost:2759")
        if no_ngrok:
            warn("This server isn't exposed into the internet if you are using firewall")
            warn("or NAT, so you'll need to use other way to expose your server.")
            warn("If you want people inside your network connect to you just do: <your-ip>:2759")
    try:
        input("Press enter to stop server...")
    except KeyboardInterrupt:
        print()
    # Stop the processes we spawned and wait for it to close.
    info("Stopping server...")
    if stk_proc.poll() is None:
        stk_proc.terminate()
        try:
            # if this returns, the process completed
            stk_proc.wait(timeout=15)
        except subprocess.TimeoutExpired:
            warn("STK doesn't exit after 15 seconds, killing process...")
            stk_proc.kill()
        stk_log.close()
    if chisel_proc and chisel_proc.poll() is None:
        chisel_proc.terminate()
        try:
            # if this returns, the process completed
            chisel_proc.wait(timeout=15)
        except subprocess.TimeoutExpired:
            warn("Chisel doesn't exit after 15 seconds, killing process...")
            chisel_proc.kill()
        chisel_log.close()
    if ngrok_proc and ngrok_proc.poll() is None:
        ngrok_proc.terminate()
        try:
            # if this returns, the process completed
            ngrok_proc.wait(timeout=15)
        except subprocess.TimeoutExpired:
            warn("ngrok doesn't exit after 15 seconds, killing process...")
            ngrok_proc.kill()
        ngrok_log.close()
          
    info("Server stopped.")
    # Close colorama
    deinit()

if __name__ == "__main__":
    main()
