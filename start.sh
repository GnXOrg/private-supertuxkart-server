#!/bin/bash

CHISEL_AMD64_URL="https://github.com/jpillora/chisel/releases/download/v1.7.6/chisel_1.7.6_linux_amd64.gz"
CHISEL_ARM64_URL="https://github.com/jpillora/chisel/releases/download/v1.7.6/chisel_1.7.6_linux_arm64.gz"
CHISEL_386_URL="https://github.com/jpillora/chisel/releases/download/v1.7.6/chisel_1.7.6_linux_386.gz"
CHISEL_ARMV7_URL="https://github.com/jpillora/chisel/releases/download/v1.7.6/chisel_1.7.6_linux_armv7.gz"

mkdir -p ./log/
echo "Starting SuperTuxKart server..."
supertuxkart --server-config=config.xml --lan-server="Vietnamese Internet" --network-console &> ./log/stk.log &
STK_PID=$!
echo "Server should be started at port 2759, else this will fail."

CHISEL_EXEC="chisel"
if ! command -v $CHISEL_EXEC &> /dev/null
then
    if [[ ! -f "./files/linux/chisel" ]]; then
        echo "Downloading chisel..."
        mkdir -p ./deps/
        CPU_ARCH=$(uname -m)
        echo "Detected $CPU_ARCH platform."
        if [ $CPU_ARCH = "x86_64" ]; then
            wget -O ./deps/chisel.gz $CHISEL_AMD64_URL
        elif [ $CPU_ARCH = "arm64" ] || [ CPU_ARCH = "aarch64" ]; then
            wget -O ./deps/chisel.gz $CHISEL_ARM64_URL
        elif [ $CPU_ARCH = "i386" ]; then
            wget -O ./deps/chisel.gz $CHISEL_386_URL
        elif [ $CPU_ARCH = "armv7l" ]; then
            wget -O ./deps/chisel.gz $CHISEL_ARMV7_URL
        else
            echo "Unsupported CPU architecture (for now)"
            exit 1
        fi
        gunzip ./deps/chisel.gz
        chmod +x ./deps/chisel
    fi
    echo "Setting up chisel..."
    CHISEL_EXEC=$(realpath ./deps/chisel)
fi
echo "Starting chisel..."
$CHISEL_EXEC server --host 127.0.0.1 &> ./log/chisel.log &
CHISEL_PID=$!
echo "Starting ngrok..."
ngrok http --region in 8080 &> ./log/ngrok.log &
NGROK_PID=$!
sleep 3
SERVER_URL="$(curl -s http://localhost:4040/api/tunnels | jq ".tunnels[0].public_url")"
echo "The **chisel** server is running on $SERVER_URL at port 2759"
echo "!!You need chisel client to connect to the server!!"
echo "You may want to check stk.log, chisel.log and ngrok.log files"
read -n 1 -s -r -p "Press any key to stop server..."
echo ""
echo "Stopping server..."
kill $NGROK_PID
kill $CHISEL_PID
kill $STK_PID
echo "Server stopped successfully"
