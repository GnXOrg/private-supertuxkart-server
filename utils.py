import functools
import platform
import stat
import gzip
import json
import requests
from colorama import Fore, Style
from shutil import copyfileobj, unpack_archive
from pathlib import Path
from tqdm.auto import tqdm

# Printing function
def warn(*args, **kwargs):
    print(Fore.YELLOW, end="")
    print(*args, **kwargs)
    print(Style.RESET_ALL, end="")
    
def error(*args, **kwargs):
    print(Fore.RED, end="")
    print(*args, **kwargs)
    print(Style.RESET_ALL, end="")

def info(*args, **kwargs):
    print(Fore.BLUE, end="")
    print(*args, **kwargs)
    print(Style.RESET_ALL, end="")

class config():
    def __init__(self, file = "./config.json"):
        self.file = Path(file)
        if self.file.exists():
            with self.file.open("r+") as f:
                self.config = json.load(f)
    
    def default(self):
        return {
            "stk": {
                "exec": "supertuxkart",
                "auto_dl": False
            },
            "chisel": {
                "exec": "chisel",
                "auto_dl": True
            },
            "ngrok": {
                "exec": "ngrok",
                "auto_dl": False
            }
        }

    def write(self, config: dict = []):
        # use self.config if config is empty
        if len(config) == 0:
            config = self.config
        with self.file.open("w+") as f:
            json.dump(config, f, indent=4)

# Workaround time.
def get_file_name(file_name):
    file_path = Path(file_name)
    out_name = None
    if ".tar" in file_name:
        out_name = file_path.with_suffix('').stem
    else:
        # One suffix
        out_name = file_path.stem
    return out_name

# Download file
def download_file(file_url, file_name):
    with requests.get(file_url, stream=True, allow_redirects=True) as r:
        if r.status_code != 200:
            error(f"Failed to download file ({r.status_code})")
        else:
            file_size = int(r.headers.get('Content-Length', 0))
            desc = "(Unknown)" if file_size == 0 else ""
            r.raw.read = functools.partial(r.raw.read, decode_content=True)  # Decompress if needed
            with tqdm.wrapattr(r.raw, "read", total=file_size, desc=desc) as r_raw:
                with Path(file_name).open("wb") as f:
                    copyfileobj(r_raw, f)

# gunzip is a gz unzipper in linux.
def gunzip(in_file, out_path: Path("./")):
    file_path = Path(in_file)
    out_path.mkdir(parents=True, exist_ok=True)
    out_file = out_path.joinpath(file_path.stem)
    with gzip.open(file_path, 'rb') as s_file, \
            out_file.open("wb") as d_file:
        copyfileobj(s_file, d_file, 65536)

def unzip(in_file, out_path: Path("./")):
    unpack_archive(in_file, out_path)

#TODO: Armv7l to Armv7 and etc.
def chisel_platform_translator():
    sys_name = platform.system().lower()
    sys_arch = platform.machine()
    if platform.machine() == "x86_64":
        sys_arch = "amd64"
    elif platform.machine() == "i386":
        sys_arch = "386"
    elif platform.machine() == "aarch64":
        sys_arch = "arm64"
    return sys_name, sys_arch

def stk_windows_platform_translator():
    sys_arch = platform.machine()
    if platform.machine() == "AMD64":
        sys_arch = "x86_64"
    elif platform.machine() == "i386":
        warn("This system is reported as i386, but the game is i686, it may not be compatible with the system.")
        sys_arch = "i686"
    elif platform.machine() == "ARM64":
        sys_arch = "aarch64"
    return sys_arch

#TODO: Armv7l to Armv7 and etc.
def stk_linux_platform_translator():
    sys_arch = platform.machine()
    if platform.machine() == "x86_64":
        sys_arch = "64bit"
    elif platform.machine() == "i386":
        sys_arch = "32bit"
    elif platform.machine() == "aarch64":
        sys_arch = "amd64"
    return sys_arch

def dl_stk():
    sys_name = None
    sys_arch = ""
    info(f"Detected platform: {platform.system()} {platform.machine()}")
    if platform.system() == "Linux":
        sys_info = stk_linux_platform_translator()
        sys_name = "linux"
        sys_arch = sys_info
    elif platform.system() == "Windows":
        sys_name = "win"
    elif platform.system() == "Darwin":
        sys_name = "mac"
    rsp = requests.get('https://api.github.com/repos/supertuxkart/stk-code/releases/latest', headers={'Accept': 'application/vnd.github.v3+json'})
    if rsp.status_code == 200:
        file_asset_url = None
        file_name = None
        for v in rsp.json()['assets']:
            if sys_name in v["name"] and sys_arch in v["name"]:
                file_name = v["name"]
                file_asset_url = v["browser_download_url"]
                break
        if file_asset_url is not None and file_name is not None:
            if not Path(file_name).exists():
                info(f"Downloading {file_name} from {file_asset_url}")
                download_file(file_asset_url, file_name)
            else:
                info("File exists, not downloading. If you want to force redownload then delete the file.")
            info("Extracting archive...")
            out_path = Path(f"./deps/stk/")
            unzip(file_name, out_path)
            Path(file_name).unlink()            
            info("Extracting completed, setting up configuration...")
            exec_path = None
            if sys_name == "linux":
                # Confusing af, but heres an explanantion
                # So this get the file name without extension, and then joinpath with the binary and then joinpathed by the out_path
                # So its result will be like: ./deps/stk/SuperTuxKart-1.3-linux-64bit/bin/supertuxkart
                exec_path = out_path.joinpath(Path(get_file_name(file_name)).joinpath(Path("./bin/supertuxkart"))).resolve()
                # Do chmod +x in Linux and maybe macOS to make sure the game is executable.
                exec_path.chmod(exec_path.stat().st_mode | stat.S_IEXEC)
            elif sys_name == "win":
                # Still confusing but you can try to figure out yourself.
                # It'll be like ./deps/stk/SuperTuxKart-1.3-win/stk-code/
                gamedir = out_path.joinpath(Path(get_file_name(file_name)).joinpath(Path("./stk-code/")))
                sys_arch = stk_windows_platform_translator()
                for f in gamedir.iterdir():
                    if sys_arch in str(f):
                        # Found matching arch version.
                        exec_path = gamedir.joinpath(f.joinpath("./bin/supertuxkart.exe"))
                        break
                if exec_path is None:
                    error("No matching architecture available for your machine.")
                    return
            elif sys_name == "mac":
                error("Not implemented yet.")
                return
            else:
                error("Unknown platform: %s" % sys_name)
                return
            info("Executable path: %s" % exec_path)
            global config
            config["stk"]["exec"] = str(exec_path)
            writeconfig()
        else:
            error("There are currently no files available for your platform.\nMaybe you can try compiling yourself.")
    else:
        error("Can't connect to the server.")

def dl_chisel():
    info(f"Detected platform: {platform.system()} {platform.machine()}")
    sys_info = chisel_platform_translator()
    sys_name = sys_info[0]
    sys_arch = sys_info[1]
    rsp = requests.get('https://api.github.com/repos/jpillora/chisel/releases/latest', headers={'Accept': 'application/vnd.github.v3+json'})
    if rsp.status_code == 200:
        file_asset_url = None
        file_name = None
        for v in rsp.json()['assets']:
            if sys_name in v["name"] and sys_arch in v["name"]:
                file_name = v["name"]
                file_asset_url = v["browser_download_url"]
                break
        if file_asset_url is not None and file_name is not None:
            info(f"Downloading {file_name} from {file_asset_url}")
            download_file(file_asset_url, file_name)
            out_path = Path(f"./deps/chisel/")
            gunzip(file_name, out_path)
            Path(file_name).unlink()
            info("Download completed, setting up configuration...")
            exec_path = out_path.joinpath(Path(file_name).stem).resolve()
            exec_path.chmod(exec_path.stat().st_mode | stat.S_IEXEC)
            global config
            config["chisel"]["exec"] = str(exec_path)
            writeconfig()
        else:
            error("There are currently no files available for your platform.\nMaybe you can try compiling yourself.")
    else:
        error("Can't connect to the server.")