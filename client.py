#!/usr/bin/python3

from shutil import which
from pathlib import Path
from colorama import init, deinit
from utils import *
import subprocess
import sys

print("Cross-platform SuperTuxKart client launcher by GnXOrg (tretrauit/darkelaina).")

def main():
    init() # initialize colorama
    cfg = config()
    # Check if configuration is empty
    if len(cfg.config) == 0:
        cfg.config = cfg.default()
        cfg.write()
        error("New configuration file created, please edit the file and relaunch.")
        sys.exit(-1)

    if which(cfg.config["stk"]["exec"]) or Path(cfg.config["chisel"]["exec"]).exists():
        info(f'Found STK executable: {cfg.config["stk"]["exec"]}')
    else:
        if not cfg.config["stk"]["auto_dl"]:
            error("""STK not found, please specify the executable path in config.json. If you want to download then change 'auto_dl' to true""")
            sys.exit(-2)
        else:
            warn("STK doens't exist, downloading...")
            dl_stk()

    # Prepare logging
    info("Preparing logging for the client...")
    log_path = Path("./log/")
    log_path.mkdir(parents=True, exist_ok=True)
    stk_log = log_path.joinpath("stk-client.log").open("w+")

    # Start processes
    info("Starting STK client...")
    # Whats the point of this script if we disable STK server starting :p
    stk_proc = subprocess.Popen([cfg.config["stk"]["exec"]], stdout=stk_log, stderr=subprocess.STDOUT)
    info("SuperTuxKart started sucessfully.")
    try:
        input("Press enter to stop server...")
    except KeyboardInterrupt:
        print()
    # Stop the processes we spawned and wait for it to close.
    info("Stopping client...")
    if stk_proc.poll() is None:
        stk_proc.terminate()
        try:
            # if this returns, the process completed
            stk_proc.wait(timeout=15)
        except subprocess.TimeoutExpired:
            warn("STK doesn't exit after 15 seconds, killing process...")
            stk_proc.kill()
        stk_log.close()

    info("Client stopped.")
    # Close colorama
    deinit()

if __name__ == "__main__":
    main()
